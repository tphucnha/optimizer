<?php
/**
 * Header type 1 for Optimizer
 *
 * Displays The Header type 1. This file is imported in header.php
 *
 * @package Optimizer
 *
 * @since Optimizer 1.0
 */
global $optimizer; ?>

<!--HEADER STARTS-->
<div class="header">
    <div class="center">
        <div class="head_inner">
            <!--LOGO START-->
            <div class="logo">
                <?php if ($logoUrl = $optimizer['logo_image_id']['url']): ?>
                    <a class="logoimga" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(home_url('/')); ?>"><img
                            src="<?php echo $logoUrl; ?>" alt="<?php bloginfo('name'); ?>" /></a>
                <?php endif; ?>

                <?php if (get_bloginfo('name')): ?>
                    <?php echo(is_home() ? '<h1>' : '<h2>') ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
                    <?php echo(is_home() ? '</h1>' : '</h2>') ?>
                <?php endif; ?>

<!--                --><?php //if (!empty(get_bloginfo('description'))): ?>
<!--                    <h4 class="desc">--><?php //echo bloginfo('description'); ?><!--</h4>-->
<!--                --><?php //endif; ?>
            </div>
            <!--LOGO END-->

            <!--MENU START-->
            <!--MOBILE MENU START-->
            <a id="simple-menu" href="#sidr"><i class="fa-bars"></i></a>
            <!--MOBILE MENU END-->

            <div id="topmenu"
                 class="<?php if ('header' == $optimizer['social_bookmark_pos']) { ?> has_bookmark<?php } ?>">
                <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
                <!--LOAD THE HEADR SOCIAL LINKS-->
                <div class="head_soc">
                    <?php if ($optimizer['social_bookmark_pos'] == 'header') { ?><?php get_template_part('framework/core', 'social'); ?><?php } ?>
                </div>
            </div>
            <!--MENU END-->

        </div>
    </div>
</div>
<!--HEADER ENDS-->